﻿using System;

namespace M4
{
    public class Module4
    {
        static void Main(string[] args)
        {
            Module4 m4 = new Module4();
            Console.WriteLine(m4.Task_1_A(new int[] {1, 2,3}));
        }

        public int Task_1_A(int[] array)
        {
            if (array is null || array.Length == 0)
            {
                throw new ArgumentNullException(nameof(array));
            }
            
            int max = array[0];
              
            for (int i = 0; i < array.Length; i++)
            {                
                if (array[i] > max)
                {
                    max = array[i];
                }                
            }
            return max;
        }

        public int Task_1_B(int[] array)
        {
            if (array is null || array.Length == 0)
            {
                throw new ArgumentNullException(nameof(array));
            }

            int min = array[0];
            
            for (int i = 1; i<array.Length; i++)
            {                
                if (array[i] < min)
                {
                    min = array[i];
                }
            }
            return min;
        }

        public int Task_1_C(int[] array)
        {
            if (array is null || array.Length == 0)
            {
                throw new ArgumentNullException(nameof(array));
            }

            int sum = 0;
            foreach (int a in array)
            {
                sum = sum + a;
            }
            return sum;
        }

        public int Task_1_D(int[] array)
        {
            if (array is null || array.Length == 0)
            {
                throw new ArgumentNullException(nameof(array));
            }

            return Task_1_A(array) - Task_1_B(array);
        }

        public void Task_1_E(int[] array)
        {
            if (array is null || array.Length == 0)
            {
                throw new ArgumentNullException(nameof(array));
            }

            int max = Task_1_A(array);
            int min = Task_1_B(array);

            for (int i = 0; i < array.Length; i++)
            {
                if (i % 2 == 0)
                {
                    array[i] = array[i] + max;
                }
                else
                {
                    array[i] = array[i] - min;
                }
            }
        }

        public int Task_2(int a, int b, int c)
        {
            return a + b + c;
        }

        public int Task_2(int a, int b)
        {
            return a + b;
        }

        public double Task_2(double a, double b, double c)
        {
            return a + b + c;
        }

        public string Task_2(string a, string b)
        {
            return a + b;
        }

        public int[] Task_2(int[] a, int[] b)
        {
            if (a is null)
            {
                throw new ArgumentNullException(nameof(a));
            }

            if(b is null)
            {
                throw new ArgumentNullException(nameof(b));
            }

            if (a.Length > b.Length)
            {              
                for (int i = 0; i < b.Length; i++)
                {
                    a[i] = a[i] + b[i];
                }
                return a;
            }
            else
            {              
                for (int i = 0; i < a.Length; i++)
                {
                    b[i] = b[i] + a[i];
                }

                return b;
            }
        }

        public void Task_3_A(ref int a, ref int b, ref int c)
        {
            a += 10;
            b += 10;
            c += 10;
        }

        public void Task_3_B(double radius, out double length, out double square)
        {
            if (radius < 0)
            {
                throw new ArgumentException(nameof(radius));
            }

            length = Math.PI * radius * 2;
            square = Math.PI * radius * radius;
        }

        public void Task_3_C(int[] array, out int maxItem, out int minItem, out int sumOfItems)
        {
            maxItem = Task_1_A(array);
            minItem = Task_1_B(array);
            sumOfItems = Task_1_C(array);
        }

        public (int, int, int) Task_4_A((int, int, int) numbers)
        {
            return (numbers.Item1 + 10, numbers.Item2 + 10, numbers.Item3 + 10);
        }

        public (double, double) Task_4_B(double radius)
        {
            if (radius < 0)
            {
                throw new ArgumentException(nameof(radius));
            }

            return (Math.PI * radius * 2, Math.PI * radius * radius);
        }

        public (int, int, int) Task_4_C(int[] array)
        {
           return (Task_1_B(array), Task_1_A(array), Task_1_C(array));
        }

        public void Task_5(int[] array)
        {
            if (array is null || array.Length == 0)
            {
                throw new ArgumentNullException(nameof(array));
            }
            for (int i = 0; i < array.Length; i++)
                array[i] += 5;
        }

        public void Task_6(int[] array, SortDirection direction)
        {
            if (array is null || array.Length == 0)
            {
                throw new ArgumentNullException(nameof(array));
            }

            int temp;
            for (int i = 0; i < array.Length - 1; i++)
            {
                for (int j = i + 1; j < array.Length; j++)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        if (array[i] > array[j])
                        {
                            temp = array[i];
                            array[i] = array[j];
                            array[j] = temp;
                        }
                    }
                    else
                    {
                        if (array[i] < array[j])
                        {
                            temp = array[i];
                            array[i] = array[j];
                            array[j] = temp;
                        }
                    }
                }
            }
        }        

        public  double Task_7(Func<double, double> func, double x1, double x2, double e, double result = 0)
        {
            if (func is null)
            {
                throw new ArgumentNullException(nameof(func));
            }

            if (e < Math.Abs(x2 - x1))
            {
                result = (x1 + x2) / 2;
            }
            else
            {
                double x = (x1 + x2) / 2;
                if (func(x1) * func(x) < 0)
                {
                    Task_7(func, x1, x, e, result);
                }
                else if (func(x2) * func(x) < 0)
                {
                    Task_7(func, x, x2, e, result);
                }
            }
                      
            return result;
        }
    }
}
